STRIP	?=	strip
INSTALL =       install
BINDIR  :=      /usr/bin

all: deps
	make -C monitors/wip
deps:
	@-if [ ! -d libbpf ]; then				\
		git clone https://github.com/libbpf/libbpf.git; \
	fi

install:
	make -C dot2/ install
	make -C monitors/wip install

clean:
	make -C monitors/wip clean

distclean: clean
	rm -rf libbpf
