// SPDX-License-Identifier: GPL-v2
#ifndef __DA_HELPERS_H
#define __DA_HELPERS_H

struct verification {
	da_t curr_state;
	char verifying;
};

enum event_set {
	EVENT = 0,
	BLOCKED,
};

struct da_event {
	int type;
	int cpu;
	pid_t pid;
	da_t curr_state;
	da_t event;
	da_t next_state;
	char safe;
	__u64 now;
};

void verification_reset(struct verification *ver)
{
	ver->verifying = 0;
	ver->curr_state = aut.initial_state;
}

void verification_start(struct verification *ver)
{
	ver->verifying = 1;
}

int verifying(struct verification *ver)
{
	return ver->verifying;
}

da_t get_curr_state(struct verification *ver)
{
	return ver->curr_state;
}

void set_curr_state(struct verification *ver, enum states state)
{
	ver->curr_state = state;
}

da_t get_next_state(enum states curr_state, enum events event)
{
	if (curr_state > state_max || curr_state < 0)
		return 0;

	if (event > event_max || event < 0)
		return 0;

	return aut.function[curr_state][event];
}

da_t *get_state_name(enum states state)
{
	if (state >= 0 && state < state_max)
		return aut.state_names[state];
	return 0;
}

da_t *get_event_name(enum events event)
{
	if (event >= 0 && event < event_max)
		return aut.event_names[event];
	return 0;
}


#define BPF_F_CURRENT_CPU 0xffffffffULL
#ifndef USER_SPACE
struct {
	__uint(type, BPF_MAP_TYPE_PERF_EVENT_ARRAY);
	__uint(key_size, sizeof(u32));
	__uint(value_size, sizeof(u32));
} events SEC(".maps");

bool process_event(u64 *ctx, struct verification *ver, enum events event)
{
	int curr_state = get_curr_state(ver);
	int next_state = get_next_state(curr_state, event);
	struct da_event da_event = {};
	bool retval = false;

	if (next_state >= 0) {
		set_curr_state(ver, next_state);
		da_event.type = EVENT;
		retval = true;
	} else {
		da_event.type = BLOCKED;
	}

	da_event.cpu = bpf_get_smp_processor_id();
	da_event.pid = bpf_get_current_pid_tgid();
	da_event.curr_state = get_curr_state(ver);
	da_event.event = event;
	da_event.next_state = next_state;
	da_event.now = bpf_ktime_get_ns();

	bpf_perf_event_output(ctx, &events, BPF_F_CURRENT_CPU, &da_event, sizeof(da_event));

	return retval;
}
#else /* USER_SPACE */

#include <sys/reboot.h>
#include <linux/reboot.h>
#include <unistd.h>

#define REACTOR_REBOOT	1

void reactor_reboot()
{
	printf("reboot...\n");
	reboot(LINUX_REBOOT_CMD_RESTART);
}

void gen_da_handle_event(void *ctx, int cpu, void *data, __u32 data_sz)
{
	const struct da_event *e = data;

	switch (e->type) {
	case EVENT:
		if (env.trace)
			printf("%llu:%llu event: %d %d %s x %s -> %s %s\n",
					e->now / 1000000000,
					e->now % 1000000000,
					e->cpu,
					e->pid,
					get_state_name(e->curr_state),
					get_event_name(e->event),
					get_state_name(e->next_state),
					e->safe ? "safe" : "");
		break;
	case BLOCKED:
		printf("%llu:%llu blocked: %d %d %s x %s \n",
				e->now / 1000000000,
				e->now % 1000000000,
				e->cpu,
				e->pid,
				get_state_name(e->curr_state),
				get_event_name(e->event));

		if (!env.reactor)
			break;

		switch(env.reactor) {
		case REACTOR_REBOOT:
			reactor_reboot();
			break;
		default:
			printf("Unknown reactor %d\n", env.reactor);
			break;
		}
	default:
		/* regular events */
		printf("unknown event\n");
	}

	fflush(NULL);
}

void gen_da_lost_events(void *ctx, int cpu, __u64 lost_cnt)
{
	printf("lost %llu events on CPU #%d!\n", lost_cnt, cpu);
}

char bpf_verbose;

static int libbpf_print_fn(enum libbpf_print_level level,
		    const char *format, va_list args)
{
	if (!bpf_verbose)
		return 0;

	return vfprintf(stderr, format, args);
}

static int bump_memlock_rlimit(void)
{
	struct rlimit rlim_new = {
		.rlim_cur	= RLIM_INFINITY,
		.rlim_max	= RLIM_INFINITY,
	};

	return setrlimit(RLIMIT_MEMLOCK, &rlim_new);
}
#endif /* USER_SPACE */
#endif /* __DA_HELPERS_H */
