#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-only
#
# dot2bpf: transform dot files into a monitor using eBPF
#
# For more information, see:
#   https://bristot.me/efficient-formal-verification-for-the-linux-kernel/
#
# Copyright 2018-2022 Red Hat, Inc.
#
# Author:
#  Daniel Bristot de Oliveira <bristot@kernel.org>

from dot2.dot2c import Dot2c
import platform
import os

class dot2bpf(Dot2c):
    monitor_types={ "per_cpu" : 1 }
    monitor_templates_dir="templates/"
    monitor_type="per_cpu"

    def __init__(self, file_path, MonitorType):
        super().__init__(file_path)

        self.monitor_type=self.monitor_types.get(MonitorType)
        if self.monitor_type == None:
            raise Exception("Unknown monitor type: %s" % MonitorType)

        self.monitor_type=MonitorType
        self.__fill_rv_templates_dir()
        self.main_c = self.__open_file(self.monitor_templates_dir + self.monitor_type + "/template.c")
        self.main_bpf_c = self.__open_file(self.monitor_templates_dir + self.monitor_type + "/template.bpf.c")
        self.makefile = self.__open_file(self.monitor_templates_dir + "/Makefile")

    def __fill_rv_templates_dir(self):

        if os.path.exists(self.monitor_templates_dir) == True:
            return

        if platform.system() != "Linux":
            raise Exception("I can only run on Linux.")

        kernel_path="/lib/modules/%s/build/tools/rv/dot2/%s" % (platform.release(), self.monitor_templates_dir)

        if os.path.exists(kernel_path) == True:
            self.monitor_templates_dir=kernel_path
            return

        if os.path.exists("/usr/share/dot2/dot2bpf/%s") == True:
            self.monitor_templates_dir="/usr/share/dot2/"
            return

        raise Exception("Could not find the template directory, do you have the kernel source installed?")


    def __open_file(self, path):
        try:
            fd = open(path)
        except OSError:
            raise Exception("Cannot open the file: %s" % path)

        content = fd.read()

        return content

    def __buff_to_string(self, buff):
        string=""

        for line in buff:
            string=string + line + "\n"

        # cut off the last \n
        return string[:-1]

    def fill_tracepoint_handlers_skel(self):
        buff=[]
        for event in self.events:
            buff.append("SEC(\"tp_btf/%s\") // XXX: Check your tracepoint name" % (event))
            buff.append("int handle__%s(u64 *ctx) // XXX: Check your tracepoint name" % (event))
            buff.append("{")
            if self.monitor_type == "hash":
                buff.append("\thandle_event(XXX: HASH, %s);" % (event));
            else:
                buff.append("\thandle_event(ctx, %s);" % (event));
            buff.append("\treturn 0;")
            buff.append("}")
            buff.append("")

        return self.__buff_to_string(buff)

    def fill_main_bpf_c(self):
        main_bpf_c = self.main_bpf_c;

        handlers_skel=self.fill_tracepoint_handlers_skel()

        main_bpf_c = main_bpf_c.replace("MODEL_NAME", self.name)
        main_bpf_c = main_bpf_c.replace("TRACEPOINT_HANDLERS_SKEL", handlers_skel)

        return main_bpf_c

    def fill_main_c(self):
        main_c = self.main_c
        main_c = main_c.replace("MIN_TYPE", "da_t")
        main_c = main_c.replace("MODEL_NAME", self.name)

        return main_c

    def fill_model_h(self):
        #
        # Adjust the definition names
        #
        buff=[]
        buff.append("#define da_t %s" % (self.get_minimun_type()))

        self.min_type="da_t"
        self.enum_states_def="states"
        self.enum_events_def="events"
        self.struct_automaton_def="automaton"
        self.var_automaton_def="aut"

        buff.extend(self.format_model())

        return self.__buff_to_string(buff)

    def fill_makefile(self):
        makefile = self.makefile
        makefile = makefile.replace("MODEL_NAME", self.name)
        return makefile

    def __create_directory(self):
        try:
            os.mkdir(self.name)
        except FileExistsError:
            return
        except:
            print("Fail creating the output dir: %s" % self.name)

    def __create_file(self, file_name, content):
        path="%s/%s" % (self.name, file_name)
        try:
            file = open(path, 'w')
        except FileExistsError:
            return
        except:
            print("Fail creating file: %s" % path)

        file.write(content)

        file.close()

    def __get_main_name(self):
        path="%s/%s" % (self.name, "main.c")
        if os.path.exists(path) == False:
           return "main.c"
        return "__main.c"

    def print_files(self):
        makefile=self.fill_makefile()
        main_bpf_c=self.fill_main_bpf_c()
        main_c=self.fill_main_c()
        model_h=self.fill_model_h()

        self.__create_directory()

        path="%s.bpf.c" % self.name
        self.__create_file(path, main_bpf_c)

        path="%s.c" % self.name
        self.__create_file(path, main_c)

        path="%s_model.h" % self.name
        self.__create_file(path, model_h)

        self.__create_file("Makefile", makefile)
