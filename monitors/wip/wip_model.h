#define da_t char
enum states {
	preemptive = 0,
	non_preemptive,
	state_max
};

enum events {
	preempt_disable = 0,
	preempt_enable,
	sched_waking,
	event_max
};

struct automaton {
	char *state_names[state_max];
	char *event_names[event_max];
	da_t function[state_max][event_max];
	da_t initial_state;
	char final_states[state_max];
};

struct automaton aut = {
	.state_names = {
		"preemptive",
		"non_preemptive"
	},
	.event_names = {
		"preempt_disable",
		"preempt_enable",
		"sched_waking"
	},
	.function = {
		{ non_preemptive,             -1,             -1 },
		{             -1,     preemptive, non_preemptive },
	},
	.initial_state = preemptive,
	.final_states = { 1, 0 },
};