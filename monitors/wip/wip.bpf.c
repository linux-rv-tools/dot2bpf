// SPDX-License-Identifier: GPL-2.0

#include "vmlinux.h"
#include <bpf/bpf_helpers.h>

#define print		bpf_printk
#define BPF_F_CURRENT_CPU 0xffffffffULL

#include "wip_model.h"
#include <da_helpers.h>

/**
 * per_cpu_vars - per-cpu maps
 */
struct {
	__uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
	__uint(max_entries, 1);
	__type(key, u32);
	__type(value, struct verification);
} per_cpu_ver SEC(".maps");

/*
 * this_cpu_ver - return the current CPU verification data
 */
static struct verification *this_cpu_ver(void)
{
	struct verification *ver;
	u32 key = 0;

	ver = bpf_map_lookup_elem(&per_cpu_ver, &key);

	return ver;
}

int handle_event(u64 *ctx, enum events event)
{
	struct verification *ver = this_cpu_ver();
	int retval;

	if (!ver)
		return 1;

	if (!verifying(ver))
		return 1;

	retval = process_event(ctx, ver, event);
	if (!retval)
		verification_reset(ver);

	return retval;
}

SEC("tp_btf/preempt_disable")
int handle__preempt_disable(u64 *ctx)
{
	handle_event(ctx, preempt_disable);
	return 0;
}

SEC("tp_btf/preempt_enable")
int handle__preempt_enable(u64 *ctx)
{
	struct verification *ver = this_cpu_ver();
	if (!ver)
		return 0;

	if (!verifying(ver)) {
		verification_start(ver);
		return 0;
	}
	handle_event(ctx, preempt_enable);
	return 0;
}

SEC("tp_btf/sched_waking")
int handle__sched_waking(u64 *ctx)
{
	handle_event(ctx, sched_waking);
	return 0;
}


char LICENSE[] SEC("license") = "GPL";
