// SPDX-License-Identifier: GPL-v2
/*
 * Copyright (c) 2021-2022 Daniel Bristot de Oliveira <bristot@kernel.org>
 */
#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <time.h>
#include <bpf/libbpf.h>
#include <bpf/bpf.h>

#define USER_SPACE 1
#define print	printf

#include "wip_model.h"

struct env {
	bool verbose;
	bool trace;
	int reactor;
} env = {
	.verbose = false,
	.trace = true,
	.reactor = 0 /* disabled */
};

#include <da_helpers.h>
#include "wip.skel.h"

struct bpf_context {
	struct perf_buffer *pb;
	struct wip_bpf *obj;
};

const char argp_program_doc[] =
"wip		XXX: ADD YOUR DESCRIPTIONo\n"
""
"USAGE: wip [-v/--verbose] [-/--notrace] [-r/--reboot]\n"
"\n";


/*
 * argp_option - command options
 *
 * XXX: Feel free to add your options here
 */
static const struct argp_option opts[] = {
	{ "verbose",	'v', NULL, 0, "Verbose debug output" },
	{ "notrace",	'n', NULL, 0, "Do not trace events (only failures)" },
	{ "reboot",	'r', NULL, 0, "Reactor: reboot" },
	{},
};

/*
 * parse_arg - parse the arguments
 *
 * The arguments are saved in the env structure and is used by this
 * program and the helpers provided by dot2ebpf.
 *
 * XXX: add the arguments as you soo wish.
 */
static error_t parse_arg(int key, char *arg, struct argp_state *state)
{
	switch (key) {
	case 'v':
		bpf_verbose = true;
		break;
	case 'n':
		env.trace = false;
		break;
	case 'r':
		env.reactor = REACTOR_REBOOT;
		break;
	default:
		return ARGP_ERR_UNKNOWN;
	};

	return 0;
}

/*
 * rv_bpf_load - load the bpf object and setup trace
 *
 * Do the basic setup for the eBPF environment, configure
 * the OS, load the bytecode, and set up the tracing session.
 *
 * XXX: you unlikely need to touch this function.
 */
int rv_bpf_load(struct bpf_context *bpf_ctx)
{
	int retval;

	libbpf_set_print(libbpf_print_fn);

	retval = bump_memlock_rlimit();
	if (retval) {
		fprintf(stderr, "failed to increase rlimit: %d", retval);
		return 1;
	}

	bpf_ctx->obj = wip_bpf__open();
	if (!bpf_ctx->obj) {
		fprintf(stderr, "failed to open and/or load BPF object\n");
		return 1;
	}

	retval = wip_bpf__load(bpf_ctx->obj);
	if (retval) {
		fprintf(stderr, "failed to load BPF object: %d\n", retval);
		return 1;
	}

	retval = wip_bpf__attach(bpf_ctx->obj);
	if (retval) {
		fprintf(stderr, "failed to attach BPF programs\n");
		return 1;
	}

	/*
	 * XXX: Feel free to provide your own tracing functions instead of using
	 * the generic ones.
	 */
	bpf_ctx->pb = perf_buffer__new(bpf_map__fd(bpf_ctx->obj->maps.events), 1024,
			gen_da_handle_event, gen_da_lost_events, NULL, NULL);

	retval = libbpf_get_error(bpf_ctx->pb);
	if (retval) {
		bpf_ctx->pb = NULL;
		fprintf(stderr, "failed to open perf buffer: %d\n", retval);
		return 1;
	}

	return 0;
}

/*
 * rv_bpf_destroy - destroy the previously loaded context
 */
void rv_bpf_destroy(struct bpf_context *bpf_ctx)
{
	if (bpf_ctx->pb)
		perf_buffer__free(bpf_ctx->pb);
	if (bpf_ctx->obj)
		wip_bpf__destroy(bpf_ctx->obj);

}

/*
 * rv_wip_loop: the main loop
 *
 * Loops reading the tracing buffer until it returns an error.
 *
 * XXX: change it as you soo wish.
 */
int rv_wip_loop(struct perf_buffer *pb)
{
	int retval;
	while ((retval = perf_buffer__poll(pb, 100)) >= 0)
		;
	return retval;
}

/*
 * main - sets up the enviroment for rv_wip_loop
 */
int main(int argc, char **argv)
{
	static const struct argp argp = {
		.options = opts,
		.parser = parse_arg,
		.doc = argp_program_doc,
	};
	struct bpf_context bpf_ctx = {
		.pb = NULL,
		.obj = NULL,
	};
	int retval;

	retval = argp_parse(&argp, argc, argv, 0, NULL, NULL);
	if (retval)
		return retval;

	retval = rv_bpf_load(&bpf_ctx);
	if (retval)
		goto destroy;

	retval = rv_wip_loop(bpf_ctx.pb);

destroy:
	rv_bpf_destroy(&bpf_ctx);

	return retval != 0;
}
