# dot2bpf

Create rv monitors usig ebpf + libbpf.

This will eventually be absorved by a meta-tool. But it is
here as an example.

# Installing

On Fedora, the user needs to install:

	sudo dnf install git glibc-devel glibc-devel.i686 bpftool clang llvm

## Building monitors

to build dot2bpf and the monitors:

	$ make
	# sudo make install

## Author
Daniel Bristot de Oliveira <bristot@kernel.org>
